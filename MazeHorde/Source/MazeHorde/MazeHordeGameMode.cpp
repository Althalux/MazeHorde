// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "MazeHordeGameMode.h"
#include "MazeHordeHUD.h"
#include "MazeHordeCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMazeHordeGameMode::AMazeHordeGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMazeHordeHUD::StaticClass();
}

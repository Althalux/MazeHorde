// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAZEHORDE_MazeHordeGameMode_generated_h
#error "MazeHordeGameMode.generated.h already included, missing '#pragma once' in MazeHordeGameMode.h"
#endif
#define MAZEHORDE_MazeHordeGameMode_generated_h

#define MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_RPC_WRAPPERS
#define MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMazeHordeGameMode(); \
	friend MAZEHORDE_API class UClass* Z_Construct_UClass_AMazeHordeGameMode(); \
public: \
	DECLARE_CLASS(AMazeHordeGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/MazeHorde"), MAZEHORDE_API) \
	DECLARE_SERIALIZER(AMazeHordeGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMazeHordeGameMode(); \
	friend MAZEHORDE_API class UClass* Z_Construct_UClass_AMazeHordeGameMode(); \
public: \
	DECLARE_CLASS(AMazeHordeGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/MazeHorde"), MAZEHORDE_API) \
	DECLARE_SERIALIZER(AMazeHordeGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MAZEHORDE_API AMazeHordeGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMazeHordeGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MAZEHORDE_API, AMazeHordeGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMazeHordeGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MAZEHORDE_API AMazeHordeGameMode(AMazeHordeGameMode&&); \
	MAZEHORDE_API AMazeHordeGameMode(const AMazeHordeGameMode&); \
public:


#define MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MAZEHORDE_API AMazeHordeGameMode(AMazeHordeGameMode&&); \
	MAZEHORDE_API AMazeHordeGameMode(const AMazeHordeGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MAZEHORDE_API, AMazeHordeGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMazeHordeGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMazeHordeGameMode)


#define MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_9_PROLOG
#define MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_RPC_WRAPPERS \
	MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_INCLASS \
	MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_INCLASS_NO_PURE_DECLS \
	MazeHorde_Source_MazeHorde_MazeHordeGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MazeHorde_Source_MazeHorde_MazeHordeGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

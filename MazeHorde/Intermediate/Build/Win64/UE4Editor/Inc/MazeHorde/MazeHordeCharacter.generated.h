// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAZEHORDE_MazeHordeCharacter_generated_h
#error "MazeHordeCharacter.generated.h already included, missing '#pragma once' in MazeHordeCharacter.h"
#endif
#define MAZEHORDE_MazeHordeCharacter_generated_h

#define MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_RPC_WRAPPERS
#define MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMazeHordeCharacter(); \
	friend MAZEHORDE_API class UClass* Z_Construct_UClass_AMazeHordeCharacter(); \
public: \
	DECLARE_CLASS(AMazeHordeCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/MazeHorde"), NO_API) \
	DECLARE_SERIALIZER(AMazeHordeCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMazeHordeCharacter(); \
	friend MAZEHORDE_API class UClass* Z_Construct_UClass_AMazeHordeCharacter(); \
public: \
	DECLARE_CLASS(AMazeHordeCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/MazeHorde"), NO_API) \
	DECLARE_SERIALIZER(AMazeHordeCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMazeHordeCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMazeHordeCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMazeHordeCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMazeHordeCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMazeHordeCharacter(AMazeHordeCharacter&&); \
	NO_API AMazeHordeCharacter(const AMazeHordeCharacter&); \
public:


#define MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMazeHordeCharacter(AMazeHordeCharacter&&); \
	NO_API AMazeHordeCharacter(const AMazeHordeCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMazeHordeCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMazeHordeCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMazeHordeCharacter)


#define MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AMazeHordeCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AMazeHordeCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AMazeHordeCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AMazeHordeCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AMazeHordeCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AMazeHordeCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AMazeHordeCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AMazeHordeCharacter, L_MotionController); }


#define MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_11_PROLOG
#define MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_RPC_WRAPPERS \
	MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_INCLASS \
	MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_INCLASS_NO_PURE_DECLS \
	MazeHorde_Source_MazeHorde_MazeHordeCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MazeHorde_Source_MazeHorde_MazeHordeCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

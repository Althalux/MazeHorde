// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MAZEHORDE_MazeHordeHUD_generated_h
#error "MazeHordeHUD.generated.h already included, missing '#pragma once' in MazeHordeHUD.h"
#endif
#define MAZEHORDE_MazeHordeHUD_generated_h

#define MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_RPC_WRAPPERS
#define MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMazeHordeHUD(); \
	friend MAZEHORDE_API class UClass* Z_Construct_UClass_AMazeHordeHUD(); \
public: \
	DECLARE_CLASS(AMazeHordeHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/MazeHorde"), NO_API) \
	DECLARE_SERIALIZER(AMazeHordeHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMazeHordeHUD(); \
	friend MAZEHORDE_API class UClass* Z_Construct_UClass_AMazeHordeHUD(); \
public: \
	DECLARE_CLASS(AMazeHordeHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/MazeHorde"), NO_API) \
	DECLARE_SERIALIZER(AMazeHordeHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMazeHordeHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMazeHordeHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMazeHordeHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMazeHordeHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMazeHordeHUD(AMazeHordeHUD&&); \
	NO_API AMazeHordeHUD(const AMazeHordeHUD&); \
public:


#define MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMazeHordeHUD(AMazeHordeHUD&&); \
	NO_API AMazeHordeHUD(const AMazeHordeHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMazeHordeHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMazeHordeHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMazeHordeHUD)


#define MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define MazeHorde_Source_MazeHorde_MazeHordeHUD_h_9_PROLOG
#define MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_RPC_WRAPPERS \
	MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_INCLASS \
	MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_INCLASS_NO_PURE_DECLS \
	MazeHorde_Source_MazeHorde_MazeHordeHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MazeHorde_Source_MazeHorde_MazeHordeHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
